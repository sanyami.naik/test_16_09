package test_16_09;

import java.io.*;
import java.util.Enumeration;
import java.util.Vector;

public class FileMerging {
    public static void main(String[] args) throws IOException {

        FileInputStream fileInputStream1 =new FileInputStream("A.txt");
        FileInputStream fileInputStream2 =new FileInputStream("B.txt");
        FileInputStream fileInputStream3 =new FileInputStream("C.txt");
        FileInputStream fileInputStream4 =new FileInputStream("D.txt");


        FileOutputStream fileOutputStream=new FileOutputStream("MergedFile.txt");
        Vector v=new Vector();
        Enumeration e=v.elements();
        v.addElement(fileInputStream1);
        v.addElement(fileInputStream2);
        v.addElement(fileInputStream3);
        v.addElement(fileInputStream4);

        SequenceInputStream sequenceInputStream=new SequenceInputStream(e);

        while(true){
            int i=sequenceInputStream.read();
            if(i==-1)
                break;


            fileOutputStream.write((char)i);
        }

        fileOutputStream.close();
        fileInputStream1.close();
        fileInputStream2.close();
        fileInputStream3.close();
        fileInputStream4.close();
        sequenceInputStream.close();
        System.out.println("Merging done succesfully!!");



        FileOutputStream fileOutputStream1=new FileOutputStream("A1.txt");
        FileOutputStream fileOutputStream2=new FileOutputStream("B1.txt");
        FileOutputStream fileOutputStream3=new FileOutputStream("C1.txt");
        FileOutputStream fileOutputStream4=new FileOutputStream("D1.txt");
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        FileInputStream fileInputStream=new FileInputStream("MergedFile.txt");


        while(true)
        {
            int j=fileInputStream.read();
            if(j==-1)
                break;
            byteArrayOutputStream.write((char)j);
        }

        byteArrayOutputStream.writeTo(fileOutputStream1);
        byteArrayOutputStream.writeTo(fileOutputStream2);
        byteArrayOutputStream.writeTo(fileOutputStream3);
        byteArrayOutputStream.writeTo(fileOutputStream4);


        byteArrayOutputStream.close();
        fileOutputStream1.close();
        fileOutputStream2.close();
        fileOutputStream3.close();
        fileOutputStream4.close();
        fileInputStream.close();
        System.out.println("Data transferred to four files suucesfully!!");




    }
}
