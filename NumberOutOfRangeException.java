package test_16_09;

public class NumberOutOfRangeException extends Exception
{
    public NumberOutOfRangeException()
    {
        System.out.println("Exception on having the sum of number out of range");
    }
}
