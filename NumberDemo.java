package test_16_09;

import java.util.Scanner;

public class NumberDemo {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String number="";
        int counter=0;
        System.out.println("Enter the string you want to check");
        String string= scanner.nextLine();
        System.out.println("Enter the number");
        int numberCheck= scanner.nextInt();
        System.out.println("The user entered number is "+numberCheck);

        char[] charArray= string.toCharArray();
        for(int i=0;i<charArray.length;i++)
        {
            if(Character.isDigit(charArray[i]))
            {
                number=number+charArray[i];
                counter++;
            }
        }

        try {

            if (counter == 0) {
                System.out.println("The string has no numbers");
                System.out.println("The sum of the number is thus zero");
                throw new NumberNotFoundException();
            }


            else
            {
                    int sum = 0;
                    int numberr = Integer.parseInt(number);
                    System.out.println("The number from string is "+numberr);
                    while (numberr > 0) {
                        int n = numberr % 10;
                        sum = sum + n;
                        numberr = numberr / 10;
                    }
                    System.out.println("The sum of number from the String is " + sum);

                    try {
                        if (numberCheck > sum)
                            throw new NumberOutOfRangeException();

                        else
                            System.out.println("The number is in range");
                    }catch(NumberOutOfRangeException n)
                    {

                    }
                }


        }catch(NumberNotFoundException n)
        {

        }




    }
}
